module blog.page.core {
    requires aisco.utility.util;
    requires vmj.routing.route;
    requires java.logging;
    exports blog.page.core;
    exports blog.page;
}
