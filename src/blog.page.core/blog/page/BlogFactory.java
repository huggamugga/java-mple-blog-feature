package blog.page;

import blog.page.core.Blog;

import java.lang.reflect.Constructor;
import java.util.logging.Logger;

public class BlogFactory {
    private static final Logger LOGGER = Logger.getLogger(BlogFactory.class.getName());

    private BlogFactory()
    {

    }

    public static Blog createBlog(String fullyQualifiedName, Object ... base) {
        Blog blog = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            blog = (Blog) constructor.newInstance(base);
        } catch (Exception e)
        {
            System.out.println(e);
            System.out.println("Failed to create instance of Blog.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(30);
        }
        return blog;
    }
}