package blog.page.core;

public abstract class BlogDecorator extends BlogComponent {
    public BlogComponent blog = null;

    public BlogDecorator (BlogComponent blog) {
        this.blog = blog;
    }

}