package blog.page.core;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import aisco.utility.util.DatabaseUtil;


public abstract class BlogComponent implements Blog {
    DatabaseUtil dbUtil = new DatabaseUtil();

    public BlogComponent() {
    
    }

    @Route(url="blog-core/create-post")
    public HashMap<String,Object> createPost(VMJExchange vmjExchange) {
        Object title = vmjExchange.getPOSTBodyForm("title");
        Object content = vmjExchange.getPOSTBodyForm("content");

        String sqlCommand = "INSERT INTO blog_page_post (title, content) VALUES ('" + title + "','" + content + "');";

        dbUtil.hitDatabase(sqlCommand);

        HashMap<String,Object> hasil = new HashMap<>();
        hasil.put("status", "succeed");
        return hasil;
    }

    @Route(url="blog-core/get-posts")
    public List<HashMap<String,Object>> getPosts(VMJExchange vmjExchange) {
        String sqlCommand = "select * from blog_page_post";
        ArrayList<String> reqFields = new ArrayList<>();
        reqFields.add("title");
        reqFields.add("content");
        List<HashMap<String,Object>> hasil = dbUtil.hitDatabaseForQueryATable(sqlCommand, reqFields);
        return hasil;
    }

    @Route(url="blog-core/get-post")
    public HashMap<String,Object> getPost(VMJExchange vmjExchange) {
        Object postId = vmjExchange.getGETParam("postid");
        String sqlCommand = "select * from blog_page_post WHERE id='" + postId + "'";
        ArrayList<String> reqFields = new ArrayList<>();
        reqFields.add("title");
        reqFields.add("content");
        HashMap<String,Object> hasil = dbUtil.hitDatabaseForQuery(sqlCommand, reqFields);
        return hasil;
    }

}