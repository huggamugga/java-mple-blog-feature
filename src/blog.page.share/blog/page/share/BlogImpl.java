package blog.page.share;
import java.util.*;
import blog.page.core.BlogDecorator;
import blog.page.core.BlogComponent;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import aisco.utility.util.DatabaseUtil;

public class BlogImpl extends BlogDecorator{
    DatabaseUtil dbUtil = new DatabaseUtil();
    private BlogComponent blogComponent;
    
    public BlogImpl(BlogComponent blogComponent) {
        super(blogComponent);
        this.blogComponent = blogComponent;
    }

    @Route(url="blog-delta-share/add-share")
    public HashMap<String,Object> addPostToShareFeature(VMJExchange vmjExchange) {
        Object postId = vmjExchange.getPOSTBodyForm("postid");
        String shareableLink = "http://localhost:8000/" + getAlphaNumericString(8);
        String sqlCommand = "INSERT INTO blog_page_share (post_id, shareable_link) VALUES ('" + postId + "', '" + shareableLink + "');";
        dbUtil.hitDatabase(sqlCommand);

        HashMap<String,Object> hasil = new HashMap<>();
        hasil.put("status", "succeed");
        return hasil;
    }

    @Override
    @Route(url="blog-delta-share/get-post")
    public HashMap<String,Object> getPost(VMJExchange vmjExchange) {
        Object postId = vmjExchange.getGETParam("postid");
    
        String sqlCommand = "SELECT blog.title as title, share.shareable_link as shareable_link FROM blog_page_post as blog, blog_page_share as share WHERE blog.id=share.post_id AND blog.id='" + postId + "';";

        ArrayList<String> reqFields = new ArrayList<>();
        reqFields.add("title");
        reqFields.add("shareable_link");

        HashMap<String,Object> hasilQuery = dbUtil.hitDatabaseForQuery(sqlCommand, reqFields);

        HashMap<String,Object> hasil = new HashMap<>();
        hasil.put("title", hasilQuery.get("title"));
        hasil.put("shareable link", hasilQuery.get("shareable_link"));
        return hasil;
    }

    public String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    } 


}