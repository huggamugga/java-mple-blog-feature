module blog.page.share {
    requires aisco.utility.util;
    requires vmj.routing.route;
    requires blog.page.core;
    exports blog.page.share;
}