module blogging.product.charity {
    exports blogging.product.charity;
    requires aisco.utility.util;
    requires vmj.routing.route;
    requires transitive blog.page.core;
    requires transitive blog.page.share;
    requires transitive blog.page.comment;
}