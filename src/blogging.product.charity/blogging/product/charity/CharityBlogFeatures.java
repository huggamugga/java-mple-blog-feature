package blogging.product.charity;

import blog.page.BlogFactory;
import blog.page.core.Blog;

import java.util.*;

public class CharityBlogFeatures {
    private ArrayList<Object> features;

    public CharityBlogFeatures() {
        this.features = new ArrayList<>();
        selectFeatures();
    }

    private void selectFeatures() {
        Blog blogShare = BlogFactory.createBlog("blog.page.share.BlogImpl", BlogFactory.createBlog("blog.page.core.BlogImpl"));

        Blog blogComment = BlogFactory.createBlog("blog.page.comment.BlogImpl", BlogFactory.createBlog("blog.page.core.BlogImpl"));

        this.features.add(blogShare);
        this.features.add(blogComment);
    }

    public ArrayList<Object> getFeatures() {
        return this.features;
    }
}