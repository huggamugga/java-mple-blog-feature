package blog.page.comment;
import java.util.*;
import blog.page.core.BlogDecorator;
import blog.page.core.BlogComponent;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import aisco.utility.util.DatabaseUtil;

import java.lang.UnsupportedOperationException;

public class BlogImpl extends BlogDecorator{
    DatabaseUtil dbUtil = new DatabaseUtil();
    private BlogComponent blogComponent;
    
    public BlogImpl(BlogComponent blogComponent) {
        super(blogComponent);
        this.blogComponent = blogComponent;
    }

    @Route(url="blog-delta-comment/add-comment")
    public HashMap<String,Object> addComments(VMJExchange vmjExchange) {
        Object postId = vmjExchange.getPOSTBodyForm("postid");
        Object comment = vmjExchange.getPOSTBodyForm("comment");

        String sqlCommand = "INSERT INTO blog_page_comment (comment, post_id) VALUES ('" + comment + "','" + postId + "');";
        dbUtil.hitDatabase(sqlCommand);

        HashMap<String,Object> hasil = new HashMap<>();
        hasil.put("status", "succeed");
        return hasil;
    }

    /**
     * remove endpoint by overriding the target method and not including the @Route annotation
     */
    @Override
    public HashMap<String,Object> createPost(VMJExchange vmjExchange) {
        throw new UnsupportedOperationException();
    }
}