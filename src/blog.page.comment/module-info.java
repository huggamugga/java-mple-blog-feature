module blog.page.comment {
    requires aisco.utility.util;
    requires vmj.routing.route;
    exports blog.page.comment;
    requires blog.page.core;
}